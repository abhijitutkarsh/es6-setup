var Person=require( './person.js');
class Teacher extends Person
{
    constructor(Name=[],Age,Gender,Interests,Subject)
    {
        super(Name,Age,Gender,Interests);
        this.Subject=Subject;
    }
    Greeting()
    {
        if(this.Gender=="M")
        {
            var str1="Hello, My name is Mr. "+this.Name[1]+", and I teach "+this.Subject+".";
            return(str1);
        }
        else{
            var str2="Hello, My name is Mrs. "+this.Name[1]+", and I teach "+this.Subject+".";
            return(str2);
        }
    }
}
class Student extends Person{
    constructor(Name=[],Age,Gender,Interests)
    {
        super(Name,Age,Gender,Interests);
        //this.Subject=Subject;
    }
    Greeting()
    {
        var str3="Yo! I\'m "+this.Name[0]+".";
        return(str3);
        

}
}
var teacher=new Teacher(["Saikat","Majumder"],24,"M","Football","Math");
var student=new Student(["Saikat","Majumder"],24,"M","Football");
module.exports = {
 ParentClass: Person,
 ChildTeacherClass: Teacher,
 ChildStudentClass: Student,
 TeacherInstance:teacher, // Name of the Object instance that you created for teacher,
 StudentInstance:student // Name of the Object instance that you created for student
 }
