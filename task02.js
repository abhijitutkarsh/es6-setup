const jobAssurance=function(nam,age,gender,experience)
{
    this.age=parseInt(age);
    this.gender=gender;
    this.experience=parseInt(experience);
    this.name=function()
    {
        if(this.gender==="M")
            return("Mr. "+nam);
        else
            return("Mrs. "+nam);
    }
    this.isEligible=function()
    {
        if(this.age>17)
            return(true);
        else
            return(false);

    }
    this.isExperienced=function()
    {
        if(this.experience>4)
            return(true);
        else
            return(false);
    }
    this.getStatus=function()
    {
        if(this.isEligible() && this.isExperienced())
        {
            return("Congratulation "+this.name()+", your job is assured");
        }
        else
        {
            return("Sorry "+this.name()+", we can not assure your job");
        }
    }
}
module.exports=jobAssurance;
