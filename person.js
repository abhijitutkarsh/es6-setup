class Person
{
    constructor(Name=[],Age,Gender,Interests)
    {
        this.Name=Name;
        this.Age=Age;
        this.Gender=Gender;
        this.Interests=Interests;
    }
    Bio()
    {
        var str=this.Name[0]+" "+this.Name[1]+" is "+this.Age+" years old. They like "+this.Interests+".";
        return(str);
    }
    Greeting()
    {
        var str2="Hi! I\'m "+this.Name[0]+" "+this.Name[1]+".";
        return(str2);
    }
}
module.exports=Person;
