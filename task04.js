class Shape
{
    constructor(name,sides,sideLength)
    {
        this.name=name;
        this.sides=sides;
        this.sideLength=sideLength;
    }
    calcPerimeter()
    {
        var sum=0;
        for(var i=1;i<=this.sides;i++)
        {
          sum=sum+this.sideLength;  
        }
        return(sum);
    }
}
class Square extends Shape
{
    constructor(sideLength)
    {
        super("square",4,sideLength);
    }
    calcArea()
    {
        return(this.sideLength*this.sideLength);
    }
}
const square=new Square(5);
var squareperim=square.calcPerimeter();
var squarearea=square.calcArea();
module.exports = {
      parentClass: Shape,
      childClass: Square, // The class name
      instance: square, // The instance name
      SquarePerimeter: squareperim, //the variable with perimeter of square,
      SquareArea: squarearea  // the variable with area of square
  }

